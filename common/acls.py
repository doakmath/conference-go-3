import json
import requests
from events import keys

def get_image(city, state):
    headers = {"Authorization": keys.PEXELS_API_KEY}
    payload = {
        "query": f'{city}, {state}, skyline',
        "orientation": "square",
        # "size": "medium",
        "color": "gray",
        "locale": "en-US",
        "page": 1,
        "per_page": 1,
    }
    response = requests.get("https://api.pexels.com/v1/search", params=payload, headers=headers)
    image = json.loads(response.content)
    img_url = image["photos"][0]["url"]
    return img_url



def get_lon_lat_by_location(city, state):
    params = {
        "appid": keys.OPEN_WEATHER_API_KEY,
        "q": f'{city}, {state}',
        "limit": 1,
    }
    response = requests.get('http://api.openweathermap.org/geo/1.0/direct', params=params)
    content = json.loads(response.content)
    if not content:
        return None, None
    lat = content[0]["lat"]
    lon = content[0]["lon"]
    return lat, lon


def weather_by_lon_lat(city, state):
    lat, lon = get_lon_lat_by_location(city, state)
    if lat == None and lon == None:
        return None
    params = {
        "lat": lat,
        "lon": lon,
        "appid": keys.OPEN_WEATHER_API_KEY,
        "units": 'imperial',
    }
    response = requests.get('https://api.openweathermap.org/data/2.5/weather', params=params)
    if response.status_code == 400:
        return None
    content = json.loads(response.content)
    weather = {}
    weather['description'] = content['weather'][0]['description']
    weather['temp'] = content['main']['temp']

    return weather
